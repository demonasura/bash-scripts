#!/bin/bash

paru -S --needed --norebuild --noredownload auto-cpufreq thermald
sudo systemctl enable --now auto-cpufreq
sudo systemctl enable --now thermald
sudo cp ./files/auto-cpufreq.conf /etc
