#!/bin/bash

paru -S --needed --norebuild --noredownload --skipreview --noconfirm --quiet bridge-utils dnsmasq virt-manager qemu-base

sudo systemctl enable --now dnsmasq
sudo systemctl enable --now libvirtd

sudo usermod -G libvirt,libvirt-qemu,qemu,rpc -a $(id -un)
