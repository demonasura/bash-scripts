#!/bin/bash

paru -S --needed --norebuild --noredownload --skipreview --noconfirm --quiet libxau libxi libxss libxtst libxcursor libxcomposite libxdamage libxfixes libxrandr libxrender mesa-libgl  alsa-lib libglvnd

printf "\nGo to https://repo.anaconda.com/archive/ \
        \n& download latest <Anaconda3-20XX.YY-Linux-x86_64.sh> \
        \nOnce downloaded, make it executable [chmod +x <downloaded file>] & run it [./<downloaded file>]\n"
