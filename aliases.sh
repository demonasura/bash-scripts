#! /bin/bash
printf "alias rm=\"rm -i\"\n" >> ~/.config/fish/config.fish
printf "alias mv=\"mv -i\"\n" >> ~/.config/fish/config.fish
printf "alias cp=\"cp -i\"\n" >> ~/.config/fish/config.fish
printf "alias update=\"paru -Syyy\"\n" >> ~/.config/fish/config.fish
printf "alias upgrade=\"paru -Syyyu\"\n" >> ~/.config/fish/config.fish
printf "alias renew=\"update && upgrade\"\n" >> ~/.config/fish/config.fish
printf "alias cp='cp -i'\n" >> ~/.bashrc
printf "alias mv='mv -i'\n" >> ~/.bashrc
printf "alias rm='rm -i'\n" >> ~/.bashrc
printf "alias update='paru -Syyy'\n" >> ~/.bashrc
printf "alias upgrade='paru -Syyyu'\n" >> ~/.bashrc
printf "alias renew='update && upgrade'\n" >> ~/.bashrc
printf "\nHouskeeping Alert!!\n\tOpen files:\n\t\t1. ~/.config/fish/config.fish\n\t\t2. ~/.bashrc\n\t & manually delete the duplicate alias lines!\n\n"
