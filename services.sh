#!/bin/bash

sudo systemctl daemon-reload
sudo systemctl enable --now bluetooth
sudo systemctl enable --now cups
sudo systemctl enable --now cronie
sudo systemctl enable --now firewalld
sudo systemctl enable --now avahi-daemon
sudo systemctl enable --now snmpd
sudo systemctl enable --now touchegg
