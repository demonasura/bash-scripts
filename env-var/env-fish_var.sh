#!/bin/bash

cp ./files/clean.sh ~/.config/clean.sh
cp ./files/boot.sh ~/.config/boot.sh
echo "Copied bash files to /home/$(id -un)/.config/"
printf "\nCopy and run following command manually:\n\t\t\t\t\tset -U clean ~/.config/clean.sh && set -U boot ~/.config/boot.sh && set -g clean ~/.config/clean.sh && set -g boot ~/.config/boot.sh\n\nOnly then the variables can be run by typing \$clean & \$boot\n"
