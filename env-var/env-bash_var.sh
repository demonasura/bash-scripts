#!/bin/bash
#use by typing $clean & $boot

cp ./files/clean.sh ~/.config/clean.sh
if  [ -z "$clean" ]
    then
        echo "clean=~/.config/clean.sh" >> ~/.bashrc
    else
        echo "Variable \$clean already exists! Skipping.."
fi

cp ./files/boot.sh ~/.config/boot.sh
if  [ -z "$boot" ]
    then
        echo "boot=~/.config/boot.sh" >> ~/.bashrc
    else
        echo "Variable \$boot already exists! Skipping.."
fi

printf "\n\nVariables can be run by typing \$clean & \$boot\n"
