# bash-scripts

## Purpose
### To make things easier while setting up a new linux installation. 
*Notice: This project is intended for use in script author's personal system running Arch Linux and may not be reprodicible in other systems.*

## Change permission

```
cd bash-scripts
chmod +x -R ./*/*.sh  ./*.sh

```
