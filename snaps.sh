#! /bin/bash

paru -S snapd --needed
sudo systemctl enable --now snapd.socket
sudo systemctl enable --now apparmor.service
sudo systemctl enable --now snapd.apparmor.service
sudo ln -s /var/lib/snapd/snap /snap
