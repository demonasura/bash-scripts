#!/bin/bash

if ! command -v paru &> /dev/null
then
    mkdir ~/.aur
    cd ~/.aur
    git clone https://aur.archlinux.org/paru-bin
    cd paru-bin
    makepkg -si
else
    echo "paru is already installed!"
fi
