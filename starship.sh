#!/bin/bash

if ! command -v starship &> /dev/null
then
    curl -sS https://starship.rs/install.sh | sh
    printf "eval \"\$(starship init bash)\" \n" >> ~/.bashrc
    echo "Starship prompt is installed!"
fi

if hash starship 2> /dev/null;
then
    echo "Setting TOML Preset to nerd-font-symbols.."
    starship preset nerd-font-symbols > ~/.config/starship.toml
    echo "Done!"
fi
