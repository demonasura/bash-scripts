#!/bin/bash

sudo cp ./files/po-config.sh /usr/bin/po-config.sh
sudo chmod a+x /usr/bin/po-config.sh
sudo cp ./files/power-off.service /etc/systemd/system/power-off.service
sudo systemctl daemon-reload
sudo systemctl enable --now power-off.service
printf "\n\nDo these things manually:\n\n\tEdit (as root) /etc/systemd/system.conf & Modify, Uncomment the line as:\n\t\t\t\t\t\t\t\t\t\tDefaultTimeoutStopSec=3s\n"
