#!/bin/bash
n=0
v[0]="Shell List:"
for x in $(chsh -l);
    do
        ((n=n+1))
        printf "\n$n) $x"
        v[$n]=$x
    done
printf "\n\nChoose shell: "
read i
if (($i >= 1)) && (($i <= $n))
then
    chsh --shell ${v[$i]} $(id -un)
else
    printf "\nPlease choose options 1 to $n from the list above!\nNo changes were made!!\tRe-run the script with correct input!!!"; exit 1
fi
