#!/bin/bash

sudo pacman -S --needed fish
chsh --shell /usr/bin/fish $(id -un)
