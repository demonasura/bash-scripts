#!/bin/bash

sudo cp ./files/grub-config.sh /usr/bin/grub-config.sh
sudo chmod a+x /usr/bin/grub-config.sh
sudo cp ./files/power-off.service /etc/systemd/system/power-off.service
sudo systemctl daemon-reload
sudo systemctl enable --now power-off.service
printf "\n\nDo these things manually:\n\n\t1) Edit (as root) /etc/default/grub & Modify the line as:\n\t\t\t\t\t\t\t\tGRUB_TIMEOUT=3\n\n\t2) Edit (as root) /etc/systemd/system.conf & Modify, Uncomment the line as:\n\t\t\t\t\t\t\t\t\t\tDefaultTimeoutStopSec=3s\n"
