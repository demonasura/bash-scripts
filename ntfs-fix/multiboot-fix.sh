#!/bin/bash

sudo cp ./files/ntfs-fix.sh /usr/bin/ntfs-fix.sh
sudo chmod a+x /usr/bin/ntfs-fix.sh
sudo cp ./files/ntfs-fix.service /etc/systemd/system/ntfs-fix.service
sudo systemctl daemon-reload
sudo systemctl enable --now ntfs-fix.service
